Deployment
----------

`Docker <https://www.docker.com/>`_ with docker-compose is used to build and deploy the project.

Every project part (back, front, mobile) has their own Dockerfile and docker-compose.yml.

In order to start different part of the projects, helper scripts are available.

For example, for the back-end API, ``start-dev.sh`` and ``start-prod.sh`` scripts execute necessary tasks before and after starting Docker. These scripts should be used instead of raw Docker (or docker-compose) commands.

A global docker-compose.yml is available and starts the whole project (back, front) and build the mobile APK.
