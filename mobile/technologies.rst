Technologies
------------

The mobile application has been built using `React Native https://facebook.github.io/react-native/`_.

React Native lets you build mobile apps using only JavaScript. It uses the same design as React, letting you compose a rich mobile UI using declarative components.


========
Packages
========

Swagapps mobile application uses the following NPM packages:

* **react-native**: meta-package for React Native
* **jsonwebtoken**: for JWT API authentication
* **react-native-vector-icons**: UI icons
