File architecture
-------

Project files are organized as follows:

::

  ── src
     ├── __tests__/
     ├── components/
     ├── helpers/
     └── screens/

Directories meaning:

* **__tests__**: holds required files for the tests
* **components**: contains UI block components (React components)
* **helpers**: global functions used for session and API
* **screens**: main UI screens code