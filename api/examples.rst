Examples
--------

These examples demonstrate the use of the API using the command-line tool `HTTPie <https://httpie.org/>`_.


Authenticate
============

``$> http https://api.swagapps.me/login email=test@test.com password=test``
