Technologies
------------

The Swagapps API has been built using `Sails JS v1 <https://sailsjs.com/>`_.

Sails makes it easy to build custom, enterprise-grade Node.js apps.


========
Packages
========

Swagapps back-end uses the following NPM packages:

* **sails**: meta-package for Sails JS
* **bcrypt**: for user password storage
* **jsonwebtoken**: for JWT API authentication
* **googleapis**: Google API implementation
