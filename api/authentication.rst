Authentication
--------------

In order to issue requests to the API, one needs to be properly authenticated.

The Swagapps API uses the `JSON Web Token <https://jwt.io/>`_ mechanism for this purpose.


=============
How to log in
=============

First, you need to issue a request to the following endpoint:

``POST /login``

with the following parameters:

:email: The user e-mail that they used to register to Swagapps
:password: The user password, in clear text
