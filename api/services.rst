Services
--------

The AREA Epitech project describes the use of Services to create Actions and REActions.

The list of services can be obtained with the following request:

``GET /services``

The ``authorization_url`` field is used to authenticate the user on the given service, and take them back to the front-end.

The **SWAG_REDIRECT_URI** string MUST be replaced by a correct (front-end dependent) callback URI that will handle the resulting (service-dependent) parameters.

Then, the front-end has to send a request to the following endpoint:

``POST /users/me/services``

with the following parameters:

- **service**: the service slug
- **code**: the code returned by the service after authenticating the user, containing tokens
- **redirect_uri**: the callback URI that SWAG_REDIRECT_URI has been replaced with
