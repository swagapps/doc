Testing
-------

The development process integrates specific tests in order to ensure a fully working application.

Tests are stored in the **src/__tests__** directory of each part of the project (api, front, mobile).

To run all tests, issue the following command::

    $ npm test
