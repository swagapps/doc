Coding style
-------

All of Swagapps code is checked with ESLint to ensure consistent content across the project.

Read more about ESLint here: `https://eslint.org/ <https://eslint.org/>`_
