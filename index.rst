Welcome to Swagapps documentation!
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Project

   authors
   deployment
   production


.. toctree::
   :maxdepth: 2
   :caption: API

   api/technologies
   api/authentication
   api/services
   api/endpoints
   api/examples


.. toctree::
   :maxdepth: 2
   :caption: Website

   front/technologies
   front/file-architecture


.. toctree::
   :maxdepth: 2
   :caption: Mobile app

   mobile/technologies
   mobile/file-architecture


.. toctree::
   :maxdepth: 2
   :caption: Quality assurance

   quality-assurance/coding-style
   quality-assurance/tests

