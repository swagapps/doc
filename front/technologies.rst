Technologies
------------

The front-end website has been built using `ReactJS 16 <https://reactjs.org/>`_.

React is a JavaScript library for building user interfaces. It is maintained by Facebook and a community of individual developers and companies.


========
Packages
========

Swagapps front-end uses the following NPM packages:

* **react**: meta-package for ReactJS
* **jsonwebtoken**: for JWT API authentication
* **axios**: AJAX requests helper
