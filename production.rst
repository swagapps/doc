Production
----------

The Swagapps project is publicly accessible over HTTPS via the following URL:

https://swagapps.me

The API is publicly reachable from:

https://api.swagapps.me
