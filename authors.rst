Authors
-------

The Swagapps project has been made available thanks to the following contributors:

* Vanissa BALAKICHENANE (Mobile developer)
* Nicolas BONTOUX (Lead mobile developer)
* Robin HOUSSAIS (Front-end developer)
* Maxime LOUET (Group leader, lead back-end developer, lead front-end developer)
* Adrien PROM TEP (Back-end developer)
